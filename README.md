composer.json:

```
"require": {
    "rekalogika/rekalogika-bundle": "dev-master"
},
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/rekalogika/rekalogika-bundle.git"
    }
]
```

AppKernel.php:

```
$bundles = array(
    new Rekalogika\Bundle\CommonBundle\RekalogikaCommonBundle()
);
```
