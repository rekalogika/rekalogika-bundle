<?php

namespace Rekalogika\Bundle\CommonBundle\Form;

use Symfony\Component\Form\AbstractType as OriginalAbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Exception;

abstract class AbstractType extends OriginalAbstractType
{
    protected function useFields(FormBuilderInterface $builder, $keepfields)
    {
        // save current fields
        $fields = $builder->all();

        // remove all fields from builder
        foreach(array_keys($fields) as $field) {
            $builder->remove($field);
        }

        // rebuild fields
        foreach ($keepfields as $keepfield) {
            if (!isset($fields[$keepfield])) {
                throw new Exception('No such field: ' . $keepfield);
            }
            $builder->add($fields[$keepfield]);
        }
    }
}