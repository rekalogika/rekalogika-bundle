$('.magicbutton').click(function() {
    var button = $(this);
    var dopost = function() {
        if (button.attr('data-method') == 'post') {
            var form = $('<form method="post"></form>');
            if (href = button.attr('href')) {
                form.attr('action', href);
            }
            if (href = button.attr('data-href')) {
                form.attr('action', href);
            }
            if (href = button.attr('data-action')) {
                form.attr('action', href);
            }
            if (csrftoken = button.attr('data-csrftoken')) {
                var csrffield = $('<input type="hidden" name="token">');
                csrffield.val(csrftoken);
                form.append(csrffield);
            }
            form.append('<input type="hidden" name="dummy" value="dummy">');
            $('body').append(form);
            form.submit();
            return false;
        }
    }

    if (confirmtxt = button.attr('data-confirm')) {
        $.confirm({
            title: 'Confirmation',
            confirmButton: 'Yes, Continue',
            cancelButton: 'No',
            confirmButtonClass: 'btn-danger',
            cancelButtonClass: 'btn-default',
            dialogClass: 'modal-dialog modal-sm',
            text: confirmtxt,
            confirm: dopost
        });
    } else {
        dopost();
    }
    return false;
});
