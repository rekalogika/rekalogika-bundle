<?php

namespace Rekalogika\Bundle\CommonBundle\Event;

class CrudEvent extends AbstractEvent
{
    protected $controller;

    /**
     * Gets the value of controller.
     *
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Sets the value of controller.
     *
     * @param mixed $controller the controller
     *
     * @return self
     */
    public function setController($controller)
    {
        $this->controller = $controller;

        return $this;
    }
}
