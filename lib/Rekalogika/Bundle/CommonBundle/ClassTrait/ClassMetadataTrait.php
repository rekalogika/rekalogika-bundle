<?php

namespace Rekalogika\Bundle\CommonBundle\ClassTrait;
use Symfony\Component\Yaml\Yaml;
use ReflectionClass;

trait ClassMetadataTrait
{
    private $classMetadataCache;

    public function getClassMetadata()
    {
        if ($this->classMetadataCache) {
            return $this->classMetadataCache;
        }

        $reflector = new ReflectionClass(get_class($this));
        $file = $reflector->getFileName();
        $dir = dirname($file);
        $base = basename($file, '.php');
        $yaml = sprintf('%s/%s.yml', $dir, $base);

        $this->classMetadataCache = Yaml::parse(file_get_contents($yaml));
        return $this->classMetadataCache;
    }
}