<?php

namespace Rekalogika\Bundle\CommonBundle\Entity;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Exception;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Doctrine\ORM\Mapping as ORM;

trait EntityTrait
{
    /**
     * set this to force entity to be dirty
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $forceDirty = '';

    protected $repository;

    public function forceDirty() {
        $this->forceDirty = rand();
        return $this;
    }

    public function fileUploadOnUpload($propertyName, $uploadDir, $uploadFilenameBase)
    {
        $fileUpload = $this->$propertyName;
        if (!$fileUpload instanceof UploadedFile) {
            return;
        }
        $this->processFileUploadOnUpload($propertyName, $fileUpload, $uploadDir, $uploadFilenameBase);
    }

    public function fileUploadOnRemove($propertyName, $uploadDir, $uploadFilenameBase)
    {
        $this->processFileUploadOnRemove($propertyName, $uploadDir, $uploadFilenameBase);
    }

    public function fileUploadOnLoad($propertyName, $uploadDir, $uploadFilenameBase)
    {
        $this->processFileUploadOnLoad($propertyName, $uploadDir, $uploadFilenameBase);
    }




    protected function processFileUploadOnUpload($propertyName, $fileUpload, $uploadDir, $uploadFilenameBase)
    {
        $fileUpload->move($uploadDir, $uploadFilenameBase);
    }

    protected function processFileUploadOnRemove($propertyName, $uploadDir, $uploadFilenameBase)
    {
        @unlink($uploadDir. '/' . $uploadFilenameBase);
    }

    protected function processFileUploadOnLoad($propertyName, $uploadDir, $uploadFilenameBase)
    {
        try {
            $this->$propertyName = new File($uploadDir.'/'.$uploadFilenameBase);
        } catch (FileNotFoundException $e) {
        }
    }



    public function setForceDirty($forceDirty) {
        $this->forceDirty = $forceDirty;
        return $this;
    }

    public function getForceDirty() {
        return $this->forceDirty;
    }

    public function getClass() {
        return strtolower(preg_replace('/^.*\W/i', '', get_class($this)));
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId()
    {
        throw new Exception('ID cannot be changed');
    }

    public function getRepository() {
        if ($this->repository) {
            return $this->repository;
        } else {
            throw new Exception('Repository not available');
        }
    }

    public function setRepository(ObjectRepository $repository) {
        $this->repository = $repository;
        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PostLoad
     */
    public function setRepositoryFromEvent(LifecycleEventArgs $event) {
        $em = $event->getEntityManager();
        $event->getEntity()->setRepository($em->getRepository(get_class($this)));
    }

    protected static function getChoiceDescription($choice, $choices)
    {
        try {
            return $choices[$choice];
        } catch (Exception $e) {
            return $choice;
        }
    }
}
