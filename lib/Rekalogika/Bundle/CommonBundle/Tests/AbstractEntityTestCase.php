<?php

namespace Rekalogika\Bundle\CommonBundle\Tests;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Rekalogika\Bundle\CommonBundle\ClassTrait\ClassMetadataTrait;

class AbstractEntityTestCase extends \PHPUnit_Framework_TestCase
{
    use ClassMetadataTrait;

    private $accessorCache;
    protected function getPropertyAccessor()
    {
        if ($this->accessorCache) {
            return $this->accessorCache;
        }
        $this->accessorCache = PropertyAccess::createPropertyAccessorBuilder()
            ->enableMagicCall()
            ->getPropertyAccessor()
        ;
        return $this->accessorCache;
    }

    protected function doTestStandardSetterGetter($object)
    {
        $metadata = $this->getClassMetadata();
        $accessor = $this->getPropertyAccessor();
        foreach ($metadata['properties'] as $property => $propertyMetadata) {
            if (!$propertyMetadata) {
                $propertyMetadata = [];
            }
            if (isset($propxertyMetadata['default'])) {
                $this->assertEquals($propertyMetadata['default'], $accessor->getValue($object, $property));
            }
            $random = $this->getRandom();
            $accessor->setValue($object, $property, $random);
            $this->assertEquals($random, $accessor->getValue($object, $property));
        }
    }

    private function getRandom()
    {
        return md5(rand());
    }
}