<?php

namespace Rekalogika\Bundle\CommonBundle\Service;
use Carbon\Carbon;

class TwigExtension extends \Twig_Extension
{
    private $securityTokenStorage;

    public function getUser()
    {
        return $this->securityTokenStorage->getToken()->getUser();
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('formatmoney', [$this, 'filterFormatMoney'], array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('formatpercent', [$this, 'filterFormatPercent']),
            new \Twig_SimpleFilter('formatdatecompact', [$this, 'filterDateCompact']),
            new \Twig_SimpleFilter('formatdatetime', [$this, 'filterDateTime'], array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('formatintervalmonth', [$this, 'filterFormatIntervalMonth']),
            new \Twig_SimpleFilter('formatfilesize', [$this, 'filterFileSize']),
            new \Twig_SimpleFilter('formatdistance', [$this, 'filterDistance']),
            new \Twig_SimpleFilter('formattimediff', [$this, 'filterFormatTimeDiff'], array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('formattimediffplain', [$this, 'filterFormatTimeDiffPlain'], array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('formattimediff2', [$this, 'filterFormatTimeDiff2'], array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('formatyearcount', [$this, 'filterFormatYearCount'], array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('formatduration', [$this, 'filterFormatDuration']),
        );
    }

    public function filterFormatMoney($amount)
    {
        if (!$amount) {
            return '-';
        } else {
            return sprintf('<span class="nobreak">Rp %s</span>', number_format($amount, 0, ',', '.'));
        }
    }

    public function filterFormatPercent($percent, $format = '%01.2f%%')
    {
        if (null === $percent) {
            return '-';
        } else {
            return sprintf($format, $percent);
        }
    }

    public function filterDateCompact($input)
    {
        if ($input instanceof \DateTime) {
            $date = $input;
        } else {
            $date = new \DateTime($input);
        }
        if ($user = $this->getUser() and $user->getTimezone()) {
            $timezone = new \DateTimeZone($user->getTimezone());
        } else {
            $timezone = new \DateTimeZone('Asia/Jakarta');
        }
        $date->setTimeZone($timezone);
        return $date->format('d M');
    }

    public function filterDateTime($input, $linebreak = false)
    {
        if ($input instanceof \DateTime) {
            $date = $input;
        } else {
            $date = new \DateTime($input);
        }
        if ($user = $this->getUser() and $user->getTimezone()) {
            $timezone = new \DateTimeZone($user->getTimezone());
        } else {
            $timezone = new \DateTimeZone('Asia/Jakarta');
        }
        $date->setTimeZone($timezone);
        if ($linebreak) {
            return $date->format('D, d M Y') . '<br>' . $date->format('H:i');
        } else {
            return $date->format('D, d M Y H:i');
        }
    }

    public function filterFormatIntervalMonth(DateInterval $interval = null)
    {
        if (!$interval) {
            return '';
        }
        $s = '';
        if ($interval->y) {
            $s .= $interval->format('%y tahun ');
        }
        if ($interval->m) {
            $s .= $interval->format('%m bulan ');
        }
        if (!$s) {
            $s .= 'dalam satu bulan';
        }
        if ($interval->invert) {
            $s .= ' ke belakang';
        }
        return $s;
    }

    public function filterFileSize($bytes)
    {
        if ($bytes < 1024) {
            return $bytes . " B";
        } elseif ($bytes < 1048576) {
            return sprintf('%.2d KB', $bytes / 1024);
        } elseif ($bytes < 1073741824) {
            return sprintf('%.2d MB', $bytes / 1048576);
        } elseif ($bytes < 1099511627776) {
            return sprintf('%.2d GB', $bytes / 1073741824);
        } else {
            return sprintf('%.2d TB', $bytes / 1099511627776);
        }
    }

    public function filterDistance($distance)
    {
        if ($distance < 1000) {
            return sprintf('%.2d m', $distance);
        } else {
            return sprintf('%.2f km', $distance / 1000);
        }
    }

    public function filterFormatTimeDiff($time)
    {
        $time = Carbon::instance($time);
        return sprintf('<span title="%s" class="label label-info">%s</span>', $time->format('D, d M Y H:i:s'), $time->diffForHumans());
    }

    public function filterFormatTimeDiffPlain($time)
    {
        $time = Carbon::instance($time);
        return $time->diffForHumans();
    }

    public function filterFormatTimeDiff2($time1, $time2)
    {
        $time1 = Carbon::instance($time1);
        $time2 = Carbon::instance($time2);
        return sprintf('<span class="label label-info">%s</span>', $time1->diffForHumans($time2, true));
    }

    public function filterFormatYearCount($numberOfMonth)
    {
        if (null === $numberOfMonth) {
            return '-';
        } elseif ($numberOfMonth < 12) {
            $string = $numberOfMonth.' bulan';
        } else {
            $yearcount = floor($numberOfMonth/12);
            $monthcount = $numberOfMonth - $yearcount * 12;
            $string = $yearcount.' tahun';
            if ($monthcount > 0) {
                $string .= ' dan '.$monthcount.' bulan';
            }
            $string .= ' ('.$numberOfMonth.' bulan)';
        }
        return $string;
    }

    public function filterFormatDuration($durationInSeconds)
    {
        $duration = '';
        $days = floor($durationInSeconds / 86400);
        $durationInSeconds -= $days * 86400;
        $hours = floor($durationInSeconds / 3600);
        $durationInSeconds -= $hours * 3600;
        $minutes = floor($durationInSeconds / 60);
        $seconds = $durationInSeconds - $minutes * 60;

        if($days > 0) {
            $duration .= $days . 'd';
        }
        if($hours > 0) {
            $duration .= ' ' . $hours . 'h';
        }
        if($minutes > 0) {
            $duration .= ' ' . $minutes . 'm';
        }
        if($seconds > 0) {
            $duration .= ' ' . $seconds . 's';
        }
        return $duration;
    }

    public function getName()
    {
        return 'rekalogika.twigextension';
    }




    // getters & setters


    /**
     * Gets the value of securityTokenStorage.
     *
     * @return mixed
     */
    public function getSecurityTokenStorage()
    {
        return $this->securityTokenStorage;
    }

    /**
     * Sets the value of securityTokenStorage.
     *
     * @param mixed $securityTokenStorage the security token storage
     *
     * @return self
     */
    public function setSecurityTokenStorage($securityTokenStorage)
    {
        $this->securityTokenStorage = $securityTokenStorage;

        return $this;
    }
}