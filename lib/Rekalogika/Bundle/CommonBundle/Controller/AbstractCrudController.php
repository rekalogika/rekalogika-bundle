<?php

namespace Rekalogika\Bundle\CommonBundle\Controller;

use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Rekalogika\Bundle\CommonBundle\Event\CrudEvent;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Exception;

abstract class AbstractCrudController extends AbstractController
{
    /* variables */

    protected $entity;
    protected $form;
    protected $formType;
    protected $pageable;
    protected $paginator;
    protected $pagination;
    protected $request;
    protected $searchForm;
    protected $searchFormType;

    /* action backends */

    protected function baseIndexAction(Request $request, $page = 1)
    {
        $this->request = $request;
        $this->searchFormType = $this->getSearchFormType();
        if ($this->searchFormType) {
            $this->searchForm = $this->createForm(
                $this->searchFormType,
                $this->getSearchFormDefaults(),
                $this->getSearchFormOptions()
            );
            if ($searchLabel = $this->getSearchLabel()) {
                $this->searchForm->add('submit', SubmitType::class, array(
                    'label' => $searchLabel
                ));
            }
            $this->searchForm->handleRequest($request);
        }
        $this->pageable = $this->getPageable($page);
        $this->paginator = $this->get('knp_paginator');
        $this->pagination = $this->paginator->paginate(
            $this->pageable,
            $page,
            $this->getPagerMaxPerPage()
        );
        $this->dispatchEvent($this->getIndexEventName());
        $this->beforeIndex();
        return $this->render($this->getIndexTemplateName(), array(
            'entities' => $this->pagination,
            'searchForm' => $this->getSearchFormView(),
        ));
    }

    protected function baseCreateAction(Request $request)
    {
        $this->request = $request;
        $this->entity = $this->createEntity();
        $this->formType = $this->getFormType();
        $this->form = $this->createForm(
            $this->formType,
            $this->entity,
            $this->getCreateFormOptions()
        );
        if ($submitLabel = $this->getSubmitLabelForCreate()) {
            $this->form->add('submit', SubmitType::class, array(
                'label' => $submitLabel
            ));
        }
        $this->dispatchEvent($this->getCreateBeforeEventName());
        $this->beforeCreate();

        if ($request->getMethod() == 'POST') {
            $this->form->handleRequest($request);
            $this->dispatchEvent($this->getCreateHandledEventName());
            $this->createBeforeValidation();
            if ($this->form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($this->entity);
                $em->flush();
                $this->dispatchEvent($this->getCreateSuccessEventName());
                if ($request->getRequestFormat() == 'json') {
                    return JsonResponse(['status' => 'ok']);
                } else {
                    if ($flash = $this->getSuccessMessageForCreate()) {
                        $this->addFlash('success', $flash);
                    }
                    return $this->redirectAfterCreate();
                }
            }
            $this->dispatchEvent($this->getCreateFailedEventName());
        }

        return $this->render($this->getCreateTemplateName(), array(
            'entity' => $this->entity,
            'form' => $this->form->createView(),
        ));
    }

    protected function baseShowAction(Request $request, $id)
    {
        $this->request = $request;
        if (!$id) {
            throw $this->createNotFoundException();
        }
        $this->entity = $this->findEntity($id);
        if (!$this->entity) {
            throw $this->createNotFoundException();
        }
        $this->denyAccessUnlessGranted('show', $this->entity);
        $this->dispatchEvent($this->getShowEventName());
        $this->beforeShow();

        return $this->render($this->getShowTemplateName(), array(
            'entity' => $this->entity
        ));
    }

    protected function baseEditAction(Request $request, $id)
    {
        $this->request = $request;
        if (!$id) {
            throw $this->createNotFoundException();
        }
        $this->entity = $this->findEntity($id);
        if (!$this->entity) {
            throw $this->createNotFoundException();
        }
        $this->denyAccessUnlessGranted('edit', $this->entity);

        $this->formType = $this->getFormType();
        $this->form = $this->createForm(
            $this->formType,
            $this->entity,
            $this->getEditFormOptions()
        );
        if ($submitLabel = $this->getSubmitLabelForEdit()) {
            $this->form->add('submit', SubmitType::class, array(
                'label' => $submitLabel
            ));
        }

        $this->dispatchEvent($this->getEditBeforeEventName());
        $this->beforeEdit();

        if ($request->getMethod() == 'POST') {
            $this->form->handleRequest($request);
            $this->dispatchEvent($this->getEditHandledEventName());
            $this->editBeforeValidation();
            if ($this->form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($this->entity);
                $em->flush();
                $this->dispatchEvent($this->getEditSuccessEventName());
                if ($flash = $this->getSuccessMessageForEdit()) {
                    $this->addFlash('success', $flash);
                }
                return $this->redirectAfterEdit();
            }
            $this->dispatchEvent($this->getEditFailedEventName());
        }

        return $this->render($this->getEditTemplateName(), array(
            'entity' => $this->entity,
            'form' => $this->form->createView(),
        ));
    }

    protected function baseDeleteAction(Request $request, $id)
    {
        return $this->baseSimpleAction(
            $request,
            $id,
            'deleteActionMethod',
            'redirectAfterDelete',
            'getSuccessMessageForDelete',

            'beforeDelete',
            null,
            null,

            'getDeleteBeforeEventName',
            'getDeleteFailedEventName',
            'getDeleteSuccessEventName',

            'delete'
        );
    }

    protected function deleteActionMethod()
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($this->entity);
        $em->flush();
    }

    protected function baseSimpleAction(
        Request $request,
        $id,
        $actionMethod,
        $redirectAfter,
        $flashSuccess,

        $hookBefore = null,
        $hookFailed = null,
        $hookSuccess = null,

        $eventBefore = null,
        $eventFailed = null,
        $eventSuccess = null,

        $voterAttribute = null
    )
    {
        $this->request = $request;
        if (!$id) {
            throw $this->createNotFoundException();
        }

        $this->checkCsrf($request, $id);

        $this->entity = $this->findEntity($id);
        if (!$this->entity) {
            throw $this->createNotFoundException();
        }
        if ($voterAttribute) {
            $this->denyAccessUnlessGranted($voterAttribute, $this->entity);
        }
        if ($eventBefore) {
            $this->dispatchEvent($this->$eventBefore());
        }
        if ($hookBefore) {
            $this->$hookBefore();
        }

        try {
            $this->$actionMethod();
        } catch(Exception $e) {
            if ($eventFailed) {
                $this->dispatchEvent($this->$eventFailed());
            }
            if ($hookFailed) {
                $this->$hookFailed();
            }
            throw $e;
        }

        if ($flash = $this->$flashSuccess()) {
            $this->addFlash('success', $flash);
        }
        if ($eventSuccess) {
            $this->dispatchEvent($this->$eventSuccess());
        }
        if ($hookSuccess) {
            $this->$hookSuccess();
        }
        return $this->$redirectAfter();
    }

    /* events */

    protected function createEvent()
    {
        $event = new CrudEvent;
        $event->setController($this);
        return $event;
    }

    protected function dispatchEvent($eventName)
    {
        $event = $this->createEvent();
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch($eventName, $event);
    }

    /* helper methods */

    protected function getPageable($page)
    {
        throw new Exception('Not implemented');
    }

    protected function createEntity()
    {
        throw new Exception('Not implemented');
    }

    protected function getFormType()
    {
        throw new Exception('Not implemented');
    }

    protected function getSearchFormType()
    {
        return null;
    }

    protected function getSearchFormDefaults()
    {
        return [];
    }

    protected function getSearchFormView()
    {
        if ($this->searchForm) {
            return $this->searchForm->createView();
        } else {
            return null;
        }
    }

    protected function findEntity($id)
    {
        throw new Exception('Not implemented');
    }

    /* form options */

    protected function getSearchFormOptions()
    {
        return [
            'method' => 'GET'
        ];
    }

    protected function getCreateFormOptions()
    {
        return [
            'method' => 'POST'
        ];
    }

    protected function getEditFormOptions()
    {
        return [
            'method' => 'POST'
        ];
    }

    /* hooks */

    protected function editBeforeValidation() {}
    protected function createBeforeValidation() {}
    protected function beforeIndex() {}
    protected function beforeCreate() {}
    protected function beforeEdit() {}
    protected function beforeShow() {}
    protected function beforeDelete() {}

    /* additional twig parameter */

    protected function getTwigParametersForShow()
    {
        return array();
    }

    protected function getTwigParametersForEdit()
    {
        return array();
    }

    protected function getTwigParametersForCreate()
    {
        return array();
    }

    protected function getTwigParametersForIndex()
    {
        return array();
    }

    protected function getTwigParametersForAll()
    {
        return array();
    }

    /* redirect after action */

    protected function redirectAfterCreate()
    {
        throw new Exception('Not implemented');
    }

    protected function redirectAfterEdit()
    {
        throw new Exception('Not implemented');
    }

    protected function redirectAfterDelete()
    {
        throw new Exception('Not implemented');
    }

    /* template names */

    protected function getIndexTemplateName()
    {
        return $this->getGenericTemplateName();
    }

    protected function getCreateTemplateName()
    {
        return $this->getGenericTemplateName();
    }

    protected function getShowTemplateName()
    {
        return $this->getGenericTemplateName();
    }

    protected function getEditTemplateName()
    {
        return $this->getGenericTemplateName();
    }

    protected function getGenericTemplateName()
    {
        $parts = preg_split('/_/', $this->request->get('_route'));
        $last = array_pop($parts);
        $first = array_shift($parts);
        if ($first != 'app') {
            array_unshift($parts, $first);
        }
        $templateName ='';
        foreach ($parts as $part) {
            $templateName .= ucfirst($part) . '/';
        }
        $templateName .= $last . '.html.twig';
        return $templateName;
    }

    /* flash messages */

    protected function getSuccessMessageForCreate()
    {
        return 'Entity has been created successfully.';
    }

    protected function getSuccessMessageForEdit()
    {
        return 'Entity has been saved successfully.';
    }

    protected function getSuccessMessageForDelete()
    {
        return 'Entity has been deleted successfully.';
    }

    /* submit buttons */

    protected function getSubmitLabelForCreate()
    {
        return 'Create';
    }

    protected function getSubmitLabelForEdit()
    {
        return 'Save';
    }

    protected function getSearchLabel()
    {
        return 'Search';
    }

    /* defaults */

    protected function getPagerMaxPerPage()
    {
        return 20;
    }

    /* eventnames */

    protected function getBaseEventName()
    {
        $parts = preg_split('/_/', $this->request->get('_route'));
        $last = array_pop($parts);
        $baseEventName = '';
        foreach ($parts as $part) {
            $baseEventName .= $part . '.';
        }
        $baseEventName = preg_replace('/\.$/', '', $baseEventName);
        return $baseEventName;
    }

    protected function getIndexEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'index');
    }

    protected function getShowEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'show');
    }

    protected function getCreateBeforeEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'create.before');
    }

    protected function getCreateHandledEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'create.handled');
    }

    protected function getCreateSuccessEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'create.success');
    }

    protected function getCreateFailedEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'create.failed');
    }

    protected function getEditBeforeEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'edit.before');
    }

    protected function getEditHandledEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'edit.handled');
    }

    protected function getEditSuccessEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'edit.success');
    }

    protected function getEditFailedEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'edit.failed');
    }

    protected function getDeleteBeforeEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'delete.before');
    }

    protected function getDeleteSuccessEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'delete.success');
    }

    protected function getDeleteFailedEventName()
    {
        return sprintf('%s.%s', $this->getBaseEventName(), 'delete.failed');
    }

    /* misc */

    protected function checkCsrf(Request $request, $id)
    {
        $csrftoken = $request->get('token');
        $csrfprovider = $this->get('security.csrf.token_manager');
        if (!$csrfprovider->isTokenValid(new CsrfToken($id, $csrftoken))) {
            $this->createAccessDeniedException();
        }
    }

    /* generic setters & getters */


    /**
     * Gets the value of entity.
     *
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Sets the value of entity.
     *
     * @param mixed $entity the entity
     *
     * @return self
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Sets the value of formType.
     *
     * @param mixed $formType the form type
     *
     * @return self
     */
    public function setFormType($formType)
    {
        $this->formType = $formType;

        return $this;
    }

    /**
     * Gets the value of form.
     *
     * @return mixed
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Sets the value of form.
     *
     * @param mixed $form the form
     *
     * @return self
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Sets the value of pageable.
     *
     * @param mixed $pageable the pageable
     *
     * @return self
     */
    public function setPageable($pageable)
    {
        $this->pageable = $pageable;

        return $this;
    }

    /**
     * Gets the value of paginator.
     *
     * @return mixed
     */
    public function getPaginator()
    {
        return $this->paginator;
    }

    /**
     * Sets the value of paginator.
     *
     * @param mixed $paginator the paginator
     *
     * @return self
     */
    public function setPaginator($paginator)
    {
        $this->paginator = $paginator;

        return $this;
    }

    /**
     * Gets the value of pagination.
     *
     * @return mixed
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * Sets the value of pagination.
     *
     * @param mixed $pagination the pagination
     *
     * @return self
     */
    public function setPagination($pagination)
    {
        $this->pagination = $pagination;

        return $this;
    }
}
