<?php

namespace Rekalogika\Bundle\CommonBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Exception;

abstract class AbstractController extends Controller
{
    protected $view = [];

    public function renderView($view, array $parameters = array())
    {
        $parameters = array_merge($this->view, $parameters);
        return parent::renderView($view, $parameters);
    }

    public function render($view, array $parameters = array(), Response $response = null)
    {
        $parameters = array_merge($this->view, $parameters);
        return parent::render($view, $parameters, $response);
    }

    public function stream($view, array $parameters = array(), StreamedResponse $response = null)
    {
        $parameters = array_merge($this->view, $parameters);
        return parent::stream($view, $parameters, $response);
    }
}
